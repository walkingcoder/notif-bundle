<?php
namespace Cpamg\NotifBundle;

class Notif
{

    protected $message;
    protected $type;
    protected $expires;
    protected $header;
    protected $horizontal;
    protected $vertical;

    public function __construct()
    {

        $this->message = "A definir";
        $this->type = false;
        $this->expires = "false";
        $this->header = false;
        $this->horizontal = false;
        $this->vertical = false;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {

        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {

        $this->message = $message;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {

        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {

        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getExpires()
    {

        return $this->expires;
    }

    /**
     * @param mixed $expires
     */
    public function setExpires($expires)
    {

        $this->expires = $expires;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeader()
    {

        return $this->header;
    }

    /**
     * @param mixed $header
     */
    public function setHeader($header)
    {

        $this->header = $header;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHorizontal()
    {

        return $this->horizontal;
    }

    /**
     * @param mixed $horizontal
     */
    public function setHorizontal($horizontal)
    {

        $this->horizontal = $horizontal;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getVertical()
    {

        return $this->vertical;
    }

    /**
     * @param mixed $vertical
     */
    public function setVertical($vertical)
    {

        $this->vertical = $vertical;

        return $this;
    }

    public function renderAjax($json = true)
    {

        $data = $this->transformArray();

        return $json ? json_encode($data) : $data;
    }

    private function transformArray()
    {

        $return['type'] = $this->getType();
        $return['message'] = $this->getMessage();
        $return['header'] = $this->getHeader();
        $return['expires'] = $this->getExpires();
        $return['horizontal'] = $this->getHorizontal();
        $return['vertical'] = $this->getVertical();

        return $return;
    }


}